package com;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class BayesianClassifier2 {

	private float[] prDigit = new float[10];
	private long[] countDigit = new long[10];
	private ArrayList<Integer> trainingLabels = new ArrayList<Integer>();
	private long numOfExamples = 0;
	private Hashtable<Integer, Grid> trainingTable = new Hashtable<Integer, Grid>();
	
	class Position{
		long spaceCount, plusCount, hashCount;
		Position(){
			
		}
	}
	
	class Grid{
		Position[][] positions = new Position[28][28];
		
		Grid(){
			for(int i=0;i<28;i++){
				for(int j=0;j<28;j++){
					positions[i][j] = new Position();
				}
			}
		}
	}
	
	public void getDigitProbabilitiesFromTrainingLabels(String trainingLabelsFile) throws Exception{
		FileInputStream fis = new FileInputStream(new File(trainingLabelsFile));
		Scanner scanner = new Scanner(fis);
		
		int digit;
		
		while(scanner.hasNext()){
			digit = scanner.nextInt();
			countDigit[digit]++;
			trainingLabels.add(digit);
			numOfExamples++;
		}
		
		for(int i=0;i<10;i++){
			prDigit[i] = (float)countDigit[i]/numOfExamples;
			System.out.println("Count of " + i + " : " + countDigit[i] + "\t Probability: " + prDigit[i]);			
		}
		System.out.println("Total labels: " + numOfExamples);
		
		scanner.close();
		fis.close();
	}
	
	public void getAttributeProbabilitiesFromTrainingData(String trainingDataFile) throws Exception {
		FileInputStream fis = new FileInputStream(new File(trainingDataFile));
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		
		for(int k=0;k < numOfExamples;k++){
			int currDigitLabel = trainingLabels.get(k);
			Grid currGrid = trainingTable.get(currDigitLabel);
			if(currGrid == null)
				currGrid = new Grid();
			
			for(int i=0;i<28;i++){
				String line = br.readLine();
				if(line == null)
					break;
				int len = line.length();
				
				for(int j=0;j<28 && j < len;j++){
					//char c = (char)br.read();
					char c = line.charAt(j);
					if( c == ' ')
						currGrid.positions[i][j].spaceCount++;
					else if (c == '+')
						currGrid.positions[i][j].plusCount++;
					else if(c == '#')
						currGrid.positions[i][j].hashCount++;
				}
			}
			
			trainingTable.put(currDigitLabel, currGrid);
		}
		br.close();
		fis.close();
	}
	
	public float getProbability(char c, int i, int j, int digit){
		Grid grid = trainingTable.get(digit);
		if(c == ' ')
			return (float)(grid.positions[i][j].spaceCount + 1)/(countDigit[digit] + 3);
		else if(c == '+')
			return (float)(grid.positions[i][j].plusCount + 1)/(countDigit[digit] + 3);
		else if(c == '#')
			return (float)(grid.positions[i][j].hashCount + 1)/(countDigit[digit] + 3 );
		
		return 0.0f;
	}
	
	public double[] prob(char[][] tempGrid, boolean ignoreSpace) throws Exception{
		double[] pr = new double[10];
		int k;
		for(k=0;k<10;k++){
			pr[k] = prDigit[k];
			//System.out.print(pr[k] + ", ");
		}
			
		for(int i=0;i<28;i++){
			for(int j=0;j<28;j++){
				char c = tempGrid[i][j];
				//if(c == ' ' && ignoreSpace)
				//	continue;
				for(k=0;k<10;k++) {
					float p = getProbability(c,i,j,k);
					pr[k] = pr[k]*p;
				}
			}
		}
		
		int maxIndex = 0;
		for(k=1;k<10;k++){
			//System.out.print(pr[k] + ", ");
			if(pr[k] > pr[maxIndex])
				maxIndex = k;
		}
		double[] result = new double[2];
		result[0] = pr[maxIndex];
		result[1] = maxIndex;
		return result;
	}
	
	public void test(String testFileName, String outputFileName) throws Exception{
		FileInputStream fis = new FileInputStream(new File(testFileName));
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		
		FileWriter out = new FileWriter(new File(outputFileName));
		
		char[][] tempGrid = new char[28][28];
		while(true){
			for(int i=0;i<28;i++){
				String line = br.readLine();
				if(line == null) {
					br.close();
					fis.close();
					out.close();
					return;
				}
				int len = line.length();
				for(int j=0;j<28 && j < len;j++){
					char c = line.charAt(j);
					tempGrid[i][j] = c;
				}
			}
			
			int maxIndex = 0;
			 
			double[] prob1 = prob(tempGrid, true);
			maxIndex = (int)prob1[1];
			out.write(maxIndex + "\n");
		}
	}
	
	public void testFunction() throws Exception{
		getDigitProbabilitiesFromTrainingLabels("trainingData/tlabel.txt");
		getAttributeProbabilitiesFromTrainingData("trainingData/tdata.txt");
		for(int k=0;k<10;k++){
			Grid grid = trainingTable.get(k);
			if(grid != null){
				for(int i=0;i<28;i++){
					for(int j=0;j<28;j++){
						System.out.print("[" + grid.positions[i][j].spaceCount + "," +
									grid.positions[i][j].plusCount + "," + 
									grid.positions[i][j].hashCount + "]  ");
					}
					System.out.println();
				}
			}
		}
	}
	
	public void compare(String myOutputLabels, String testLabels) throws Exception{
		FileReader reader1 = new FileReader(new File(myOutputLabels));
		FileReader reader2 = new FileReader(new File(testLabels));
		long totalCount = 0, matchCount = 0;
		int[] actualCharCount = new int[10];
		int[] myCharCount = new int[10];
		int zeroChar = 48;
		Hashtable<Character, Integer> table = new Hashtable<Character, Integer>();
		do {
			int c1 = reader1.read();
			int c2 = reader2.read();

			if(c1 == -1 || c2 == -1)
				break;
			else if(c1 == 10 && c2 == 10)
				continue;
			if(c1 == c2)
				matchCount++;

			int currTestLabel = (int)c2 - zeroChar;
				
			actualCharCount[currTestLabel]++;			
			int currMyLabel = (int)c1 - zeroChar;
			if(currMyLabel == currTestLabel)
				myCharCount[currTestLabel]++;
			totalCount++;
		} while(true);
		
		reader1.close();
		reader2.close();
		System.out.println("Overall accuracy: " + ((double)matchCount/totalCount)*100.0);
		
		System.out.println("Accuracy of each digit: ");
		DecimalFormat decFormat = new DecimalFormat("#.##");
		for(int i=0;i<10;i++){
			//System.out.println(i + ": " + decFormat.format((myCharCount[i]*100.0)/actualCharCount[i]));
			System.out.println(decFormat.format((myCharCount[i]*100.0)/actualCharCount[i]));
		}
	}
	
	public static void main(String args[]) throws Exception{
		BayesianClassifier2 b = new BayesianClassifier2();
		//b.testFunction();
		System.out.println("Reading test labels...");
		b.getDigitProbabilitiesFromTrainingLabels("trainingData/traininglabels");
		System.out.println("Reading test data...");
		b.getAttributeProbabilitiesFromTrainingData("trainingData/trainingimages");
		System.out.println("Testing...");
		b.test("trainingData/testimages2.txt", "trainingData/testOut.txt");
		b.compare("trainingData/testOut.txt", "trainingData/testlabels");
		//System.out.println("Result digit: " + b.getTargetDigit("trainingData/test.txt"));
	}
}

